$(function() {
	// Slider
	var period = ["1940", "1950", "1960", "1970", "1980", "1990", "2000", "2010"];

	// Porcentajes
	var aumento = 100 / (period.length - 1);
	var percents = [0];
	var gradient = [];
	gradient[0] = float2color(0);

	for (var i = 1; i < period.length - 1; i++) {
		var dec = percents[i-1] + aumento;
		var n = dec.toFixed(3);
		percents[i] = Number(n);
		gradient[i] = float2color(percents[i]/100);
	}
	percents[period.length-1] = 100;
	gradient[period.length-1] = float2color(1);
	gradient.reverse();
	
	// Slider
	$( "#slider" ).slider({
		range: true,
		min: 0,
		max: period.length-1,
		create: function ( event, ui ) {
			// create our first pip
			var collection = createLabelSlider("first");
			// for every stop in the slider; we create a pip.
			for( var i = 1; i < period.length - 1; i++ ) {
		    	collection += createLabelSlider( i );
			}
			// create our last pip
			collection += createLabelSlider("last");
			$(this).append(collection);
		},
		slide: function( event, ui ) {
			$("#slider .ui-slider-pip").removeClass('active');
			$("#slider .ui-slider-pip-" + (ui.values[0]+1) ).addClass('active');
			$("#slider .ui-slider-pip-" + (ui.values[1]+1) ).addClass('active');
		}
    });

    function createLabelSlider( which ) {
        var pips = period.length;
        var label,
            percent,
            classes = "ui-slider-pip",
            css = "",
            css_line = "",
            color = "";

        // First Pip on the Slider
        if ( "first" === which ) {
            label = period[0];
            percent = percents[0];
            classes += " ui-slider-pip-1 ui-slider-pip-first";
            color = gradient[0];
        // Last Pip on the Slider
        } else if ( "last" === which ) {
            label = period[pips-1];
            percent = percents[pips-1];
            classes += " ui-slider-pip-"+pips + " ui-slider-pip-last";
            color = gradient[pips-1];
        // All other Pips
        } else {
            label = period[which];
            percent = percents[which];
            classes += " ui-slider-pip-"+(which+1);
            color = gradient[which];
        }

        css = "left: " + percent + "%; width: " + percents[1] + "%;";
        css_line = "left: " + parseInt(percents[1]) + "%; background: " + color + ";";
        // add this current pip to the collection
        return  "<div class=\""+classes+"\" style=\""+css+"\">"+
                    "<span class=\"ui-slider-line\" style=\""+css_line+"\"></span>"+
                    "<span class=\"ui-slider-label\">"+ label +"</span>"+
                "</div>";
    }

    function float2color( percentage ) {
	    var color_part_dec = 255 * percentage;
	    var color_part_hex = Number(parseInt( color_part_dec , 10)).toString(16);
	    return "#" + color_part_hex + color_part_hex + color_part_hex;
	}

});